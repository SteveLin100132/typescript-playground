import { PaymentStrategy } from './payment.strategy';

export class CreditCardStrategy implements PaymentStrategy {
  constructor() {}

  public pay(custom: string, price: number): string {
    return `${custom} pay $${price} by credit card`;
  }
}