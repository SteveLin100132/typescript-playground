import { PaymentStrategy } from './payment.strategy';

export class Custom {
  constructor(
    public name: string,
    public product = new Map<string, number>()
  ) {}

  buy(item: string, price: number): Custom {
    this.product.set(item, price);
    return this;
  }

  pay(payment: PaymentStrategy): void {
    const total = Array.from(this.product)
      .map(item => item[1])
      .reduce((accumulator, current) => accumulator + current);
    console.log(payment.pay(this.name, total));
  }
}