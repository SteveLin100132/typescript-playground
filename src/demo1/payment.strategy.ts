export interface PaymentStrategy {
  pay(custom: string, price: number): string;
}