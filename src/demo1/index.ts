import { Custom } from './custom';
import { CashStrategy } from './cash.strategy';
import { CreditCardStrategy } from './credit-card.strategy';

const custom = new Custom('Steve')
  .buy('PC', 30000)
  .buy('Smart Phone', 6500);

custom.pay(new CashStrategy());
custom.pay(new CreditCardStrategy());

