import { PaymentStrategy } from './payment.strategy';

export class CashStrategy implements PaymentStrategy {
  constructor() { }

  pay(custom: string, price: number): string {
    return `${custom} pay $${price} by cash`;;
  }
}